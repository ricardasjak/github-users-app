This project was bootstrapped with [Create React App with Typescript](https://github.com/Microsoft/TypeScript-React-Starter#create-our-new-project).

## Standard commands (install, run, test, build release)
```
yarn install
yarn start
yarn test
yarn build
```
## Notes regarding Github API integration

Github provides limited access to their API for anonymous users. 
Something like 60 requests per hour from single IP. So extensive use of application
may temporarily be blocked by Github server.

Pagination was hardest part to solve. However Github api does not provide
Link header previous "since" value, which makes pagination more difficult
to implement.

## Development notes

It's always difficult to find right balance between enterprise level production-ready code,
and simple, yet effective code to meet requirements of the task. This project could be always improved,
and as time goes on, more and more improvements should be done.

Due to time constrains, I have left quite few "todos" all over the source code. Sometimes
it would just take another day to come up with better solution, or discuss a thing with
colleague, what sadly is not so possible.

Most interesting and hardest problem was to implement modern pagination - infinity scroll for users list.
It works OK, however simple/classic Previous/Next page pagination could be much simpler, and perhaps more reliable,
more performance efficient solution. Speaking about infinity scroll performance, I could not quickly implement
partial users list trimming (rendering only latest data, and dropping out previous pages to 
save rendering time and memory). Proper virtual pagination could be another coding challenge itself ;-)

CSS should be converted to SASS, and I still have reserved opinion about Styled React components.
However I plan to investigate it more deeply in coming feature.

## Author
Ricardas Jaksebaga
