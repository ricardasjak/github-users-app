import * as React from 'react';

interface UsersListItemProps {
  user: User,
  onUserSelection: (username: string) => void
}

const UsersListItem: React.StatelessComponent<UsersListItemProps> = (props) => {
  const {user: {avatarUrl, username}, onUserSelection} = props;
  const handleClick = (e: any) => {
    e.preventDefault();
    onUserSelection(username);
  };
  return (
    <div onClick={handleClick}
         className="users-list-item">
      <img src={avatarUrl} className="users-list-item-avatar"/>
      <span>{username}</span>
    </div>
  );
};

export default UsersListItem;
