import * as enzyme from 'enzyme';
import * as React from 'react';
import UsersList from './UsersList';

describe('UsersList', () => {
  const onSelection = (username: string) => undefined;
  const onLoadMore = () => undefined;

  test('UsersList -- should render with empty list class', () => {
    const usersList = enzyme.shallow(<UsersList onUserSelection={onSelection}
                                                onLoadMore={onLoadMore}
                                                hasMore={false}
                                                users={[]}/>);
    expect(usersList.text()).toEqual('No users data');
  });
});


