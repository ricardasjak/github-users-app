import * as React from 'react';

// @ts-ignore
import InfiniteScroll from 'react-infinite-scroller';

import './UsersList.css';
import UsersListItem from './UsersListItem';

interface UsersListProps {
  users: User[],
  hasMore: boolean,
  onUserSelection: (username: string) => void,
  onLoadMore: () => void,
}

class UsersList extends React.PureComponent<UsersListProps> {

  public render() {
    const {users, hasMore, onLoadMore, onUserSelection} = this.props;
    if (users.length === 0) {
      return <React.Fragment>No users data</React.Fragment>;
    }
    return (
      <div className="users-list">
        <h2>Users list</h2>

        {/*documentation: https://www.npmjs.com/package/react-infinite-scroller*/}
        <InfiniteScroll
          pageStart={0}
          loadMore={onLoadMore}
          hasMore={hasMore}
          useWindow={true}
          className="users-list-container"
        >
          {users.map((user, index) =>
            <UsersListItem
              key={index}
              user={user}
              onUserSelection={onUserSelection}
            />
          )}
        </InfiniteScroll>
      </div>
    );
  }
}

export default UsersList;
