import {RootStoreType} from '../../App/store';

// This function may not make much sense, and its logic could be moved to the caller (parent) function.
// Unless we decide to tweak pagination for performance reasons and slice returning array.
// Then it might make sense to keep extra logic here.
const selectUsers = (state: RootStoreType): User[] => {
  const {users} = state.usersStore;
  if (users === undefined) {
    return [];
  }
  return users;
};

export default selectUsers;
