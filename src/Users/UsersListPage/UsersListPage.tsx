import * as React from 'react';
import {connect} from 'react-redux';

import {push} from 'connected-react-router';
import {usersListLoadAction} from '../usersStore';
import selectUsersListPageProps from './selectUsersListPageProps';
import UsersList from './UsersList/UsersList';

export interface UsersListPageProps {
  users: User[],
  hasMore: boolean,
}

class UsersListPage extends React.Component<UsersListPageProps> {

  public componentDidMount() {
    // There are at least 4 ways how to access actions from React-Redux container components
    // I chose simplest approach.

    // @ts-ignore
    const { dispatch, users } = this.props;
    if (users.length === 0) {
      dispatch(usersListLoadAction());
    }
  }

  // todo: implement loading spinner
  public render() {
    const {users, hasMore} = this.props;
    return (
      <UsersList
        users={users}
        hasMore={hasMore}
        onUserSelection={this.handleProgramSelection}
        onLoadMore={this.handleLoadMore}
      />
    );
  }

  private handleProgramSelection = (username: string) => {
    // @ts-ignore
    this.props.dispatch(push(`/users/${username}`));
  };

  private handleLoadMore = () => {
    // @ts-ignore
    const { dispatch } = this.props;
    dispatch(usersListLoadAction());
  }
}

export default connect(selectUsersListPageProps)(UsersListPage);
