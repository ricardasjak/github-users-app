import {RootStoreType} from '../../App/store';
import selectUsers from './selectUsers';
import {UsersListPageProps} from './UsersListPage';

// todo: add tests
const selectUsersListPageProps = (state: RootStoreType): UsersListPageProps => {
  const users = selectUsers(state);
  const {hasMore} = state.usersStore;

  return {
    hasMore,
    users,
  };
};

export default selectUsersListPageProps;
