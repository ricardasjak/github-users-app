import {RootStoreType} from '../../App/store';
import {UserDetailsPageProps} from './UserDetailsPage';

const selectUserDetailsPageProps = (state: RootStoreType): UserDetailsPageProps => ({
  userDetails: state.usersStore.selectedUserDetails,
});

export default selectUserDetailsPageProps;
