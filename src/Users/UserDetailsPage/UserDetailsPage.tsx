import {push} from 'connected-react-router';
import * as React from 'react';
import {connect} from 'react-redux';
import {userDetailsLoadAction} from '../usersStore';
import selectUserDetailsPageProps from './selectUserDetailsPageProps';
import UserDetailsComponent from './UserDetails';

export interface UserDetailsPageProps {
  userDetails?: UserDetails
}

class UserDetailsPage extends React.Component<UserDetailsPageProps> {

  public componentDidMount() {
    // @ts-ignore
    const {dispatch, match: {params}} = this.props;
    dispatch(userDetailsLoadAction(params.username));
  }

  public render() {
    const {userDetails} = this.props;
    return (
      <UserDetailsComponent
        userDetails={userDetails}
        navigateBack={this.handleBackClick}/>
    );
  }

  private handleBackClick = () => {
    // @ts-ignore
    this.props.dispatch(push('/users'));
  };
}

export default connect(selectUserDetailsPageProps)(UserDetailsPage);
