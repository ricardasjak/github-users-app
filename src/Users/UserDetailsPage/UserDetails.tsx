import * as React from 'react';

import './UserDetails.css';
import UserDetailsItem from './UserDetailsItem';

interface UserDetailsProps {
  userDetails?: UserDetails,
  navigateBack: () => void,
}

const labelAndValue = (label: string, value: string | React.ReactNode) => (
  <UserDetailsItem>
    <div className="user-details-item-label">{label}</div>
    <div>{value}</div>
  </UserDetailsItem>
);

class UserDetailsComponent extends React.PureComponent<UserDetailsProps> {

  public render() {
    const {userDetails, navigateBack} = this.props;
    if (userDetails === undefined) {
      return <React.Fragment>User does not exist.</React.Fragment>;
    }
    const {avatarUrl, blog, company, email, location, name, username} = userDetails;

    return (
      <div className="user-details">
        <h2 className="user-details-header">{username}</h2>
        <img src={avatarUrl} className="user-details-avatar"/>

        {labelAndValue('User name', username)}
        {labelAndValue('Name', name)}
        {labelAndValue('Location', location)}
        {labelAndValue('Company', company)}
        {labelAndValue('Email', email)}
        {labelAndValue('Blog', <a href={blog} target="_blank">{blog}</a>)}

        <a className="user-details-button-back"
            onClick={navigateBack}>
            Back to users list
        </a>
      </div>
    );
  }
}

// todo: think about better solution to solve naming conflict between interface and component
export default UserDetailsComponent;
