import * as React from 'react';

const UserDetailsItem: React.StatelessComponent = (props) => {
  const {children} = props;
  return (
    <div className="user-details-item">
      {children}
    </div>
  );
};

export default UserDetailsItem;
