import usersStore, {
  userDetailsLoadedAction,
  usersListLoadedAction,
} from './usersStore';

describe('usersStore', () => {

  const hasMore = true;
  const nextSince = 0;

  describe('usersListLoadedAction', () => {
    let users: User[];
    let action: any; // todo: set action type
    beforeEach(() => {
      users = [{
        avatarUrl: 'avatar',
        id: 1,
        username: 'pet3r',
      }];
      action = usersListLoadedAction({users, nextSince});
    });

    test('should create USERS_LIST_LOADED action', () => {
      expect(action).toEqual({
        payload: {nextSince, users},
        type: 'USERS_LIST_LOADED',
      });
    });

    test('should set users array by value', () => {
      const newState = usersStore({hasMore, nextSince}, action);
      expect(newState.users).toEqual(users);
    });

    test('should NOT set users array by reference', () => {
      const newState = usersStore({hasMore, nextSince}, action);
      expect(newState.users).not.toBe(users);
    });
  });

  describe('userDetailsLoadedAction', () => {
    let userDetails: UserDetails;
    let action: any; // todo: set action type

    beforeEach(() => {
      userDetails = {
        avatarUrl: 'avatar',
        blog: '',
        company: '',
        email: 'email@email',
        id: 1,
        location: 'Geneva',
        name: 'Peter',
        username: 'pet3r',
      };
      action = userDetailsLoadedAction(userDetails);
    });

    test('should create USERS_LIST_USER_LOADED action', () => {
      expect(action).toEqual({
        payload: {...userDetails},
        type: 'USERS_LIST_USER_LOADED',
      });
    });

    test('should set userDetails by value', () => {
      const newState = usersStore({hasMore, nextSince}, action);
      expect(newState.selectedUserDetails).toEqual(userDetails);
    });

    test('should NOT set userDetails by reference', () => {
      const newState = usersStore({hasMore, nextSince}, action);
      expect(newState.selectedUserDetails).not.toBe(userDetails);
    });
  });

  // todo: add more tests on other actions (especially thunks) and action creators.
});
