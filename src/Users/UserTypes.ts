interface User {
  id: number,
  avatarUrl: string,
  username: string
}

interface UserDetails extends User {
  name: string,
  company: string,
  blog: string,
  email: string,
  location: string,
}
