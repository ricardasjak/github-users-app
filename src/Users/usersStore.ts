import * as usersApi from '../api/usersApi';

// todo: lack of TS support on action creators
// todo: check // https://github.com/piotrwitek/react-redux-typescript-guide#action-creators

export interface UsersStoreType {
  users?: User[],
  hasMore: boolean,
  nextSince: number,
  selectedUserDetails?: UserDetails,
}

// Primary advantage of having "everything" in single store source file is to keep redux boilerplate more compact.
// However this is very subjective decision. Depending on real project needs, store source size,
// and store re-usability across different features/modules,
// it might be wise to extract actions, creators and types into separate source file(s).

const usersActionTypes = {
  USERS_LIST_LOAD: 'USERS_LIST_LOAD',
  USERS_LIST_LOADED: 'USERS_LIST_LOADED',
  USERS_LIST_USER_LOAD: 'USERS_LIST_USER_LOAD',
  USERS_LIST_USER_LOADED: 'USERS_LIST_USER_LOADED',
};

/* action creators */
const usersListStartLoadAction = () => ({
  type: usersActionTypes.USERS_LIST_LOAD
});

export const usersListLoadedAction = (payload: {users: User[], nextSince: number}) => ({
  payload,
  type: usersActionTypes.USERS_LIST_LOADED,
});

const userDetailsStartLoadAction = () => ({
  type: usersActionTypes.USERS_LIST_USER_LOAD
});

export const userDetailsLoadedAction = (payload: UserDetails) => ({
  payload,
  type: usersActionTypes.USERS_LIST_USER_LOADED,
});

/* actions */
export const usersListLoadAction = () => {
  // @ts-ignore
  return (dispatch, getState) => {
    dispatch(usersListStartLoadAction());
    const nextSinceCurrent = getState().usersStore.nextSince;
    usersApi.getUsers(nextSinceCurrent)
      .then(({users, nextSince}) => dispatch(usersListLoadedAction({users, nextSince})));
  };
};

export const userDetailsLoadAction = (username: string) => {
  // @ts-ignore
  return (dispatch) => {
    dispatch(userDetailsStartLoadAction());
    usersApi.getUserDetails(username)
      .then(userDetails => dispatch(userDetailsLoadedAction(userDetails)))
      .catch(error => console.error(error.message));
  };
};

/* reducers */
// @ts-ignore
export default (oldState: UsersStoreType = {}, {type, payload}) => {
  const newState: UsersStoreType = {
    ...oldState,
  };
  switch (type) {
    case usersActionTypes.USERS_LIST_LOAD: {
      // todo: if user loading fails for some reason we may have a problem here,
      // todo: hasMore = false would prevent any attempt to reload.
      newState.hasMore = false;
      return newState;
    }
    case usersActionTypes.USERS_LIST_LOADED: {
      // unfortunately github api does not provide reliable way to know last page,
      // and users count is constantly increasing over time.
      newState.hasMore = payload.users.length > 0;
      newState.users = [...newState.users || [],...payload.users];
      newState.nextSince = payload.nextSince || oldState.nextSince;
      return newState;
    }
    case usersActionTypes.USERS_LIST_USER_LOAD: {
      newState.selectedUserDetails = undefined;
      return newState;
    }
    case usersActionTypes.USERS_LIST_USER_LOADED: {
      const userDetails = payload as UserDetails;
      newState.selectedUserDetails = {...userDetails};
      return newState;
    }
    default:
      return newState;
  }
}
