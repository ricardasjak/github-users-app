import {connectRouter, routerMiddleware} from 'connected-react-router';
import {createBrowserHistory} from 'history';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import usersStore, {UsersStoreType} from '../Users/usersStore';

export interface RootStoreType {
  usersStore: UsersStoreType
}

const rootReducer = combineReducers({
  usersStore
});

export const history = createBrowserHistory();

export default function configureStore() {
  return createStore(
    connectRouter(history)(rootReducer),
    composeWithDevTools(
      applyMiddleware(routerMiddleware(history)),
      applyMiddleware(thunk),
    )
  );
}
