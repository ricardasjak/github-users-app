import * as React from 'react';

import { ConnectedRouter } from 'connected-react-router';
import {History} from 'history';
import routes from './routes';

import './App.css';

interface AppProps {
  history: History;
}

const App = ({ history }: AppProps) => {
  return (
    <ConnectedRouter history={history}>
      <div className="App">
        <header>
          <h1>Github users</h1>
        </header>
        { routes }
      </div>
    </ConnectedRouter>
  )
};

export default App;
