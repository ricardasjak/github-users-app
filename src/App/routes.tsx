import * as React from 'react';
import {Route, Switch} from 'react-router';
import UsersDetailsPage from '../Users/UserDetailsPage/UserDetailsPage';
import UsersListPage from '../Users/UsersListPage/UsersListPage';

const routes = (
  <Switch>
    <Route exact={true} path="/" component={UsersListPage}/>
    <Route exact={true} path="/users" component={UsersListPage}/>
    <Route exact={true} path="/users/:username" component={UsersDetailsPage}/>
  </Switch>
);

export default routes;
