import makeUser from "./makeUser";

test('makeUser -- should make', () => {
  const userDto: UserDto = {
    avatar_url: 'avatar',
    id: 1,
    login: 'login'
  };

  const expected: User = {
    avatarUrl: 'avatar',
    id: 1,
    username: 'login'
  };

  expect(makeUser(userDto)).toEqual(expected);
});
