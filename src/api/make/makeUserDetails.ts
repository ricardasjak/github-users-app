import makeUser from './makeUser';

const makeUserDetails = (userDetailsDto: UserDetailsDto): UserDetails => {
  const { blog, company, email, location, name} = userDetailsDto;
  // preferred way is not to have null'able props
  return {
    ...makeUser(userDetailsDto),
    blog: blog || '',
    company: company || '',
    email: email || '',
    location: location || '',
    name: name || '',
  }
};

export default makeUserDetails;
