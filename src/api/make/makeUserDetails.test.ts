import makeUserDetails from "./makeUserDetails";

test('makeUserDetails -- should make', () => {
  const userDetailsDto: UserDetailsDto = {
    avatar_url: 'avatar',
    id: 1,
    location: 'New York',
    login: 'login',
    name: 'name',
  };

  const expected: UserDetails = {
    avatarUrl: 'avatar',
    blog: '',
    company: '',
    email: '',
    id: 1,
    location: 'New York',
    name: 'name',
    username: 'login',
  };

  expect(makeUserDetails(userDetailsDto)).toEqual(expected);
});
