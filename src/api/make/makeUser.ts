const makeUser = (userDto: UserDto): User => {
  const {avatar_url, id, login} = userDto;
  return {
    avatarUrl: avatar_url || '',
    id,
    username: login
  }
};

export default makeUser;
