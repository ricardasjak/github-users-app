import baseUrl from './baseUrl';
import makeUser from './make/makeUser';
import makeUserDetails from './make/makeUserDetails';

// todo: unit test this ugliness... ;-/
function parseNextSinceValue(nextLinkHeader: string): number {
  if (!nextLinkHeader) {
    return 0;
  }
  // example value
  // <https://api.github.com/users?since=151&per_page=15>; rel="next", <https://api.github.com/users{?since}>; rel="first"
  nextLinkHeader = nextLinkHeader.split(', ')
    .find(link => link.includes('rel=\"next\"')) || '';

  nextLinkHeader = nextLinkHeader.split(';')[0]
    .replace('<', '')
    .replace('>', '');

  const queryParams = nextLinkHeader.split('?')[1];
  const pairs = queryParams.split('&');

  const sinceKeyValuePair = pairs.find(pair => pair.includes('since'));
  const sinceValue = sinceKeyValuePair ? sinceKeyValuePair.split('=')[1] : '';
  return parseInt(sinceValue, 10) || 0;
}

// pageSize = 48, renders well in many cases, because it nicely fits in rows by 2, 3, 4, 6, 8 or 12 items per row.
export const getUsers = (
  since: number = 0,
  pageSize: number = 48
): Promise<{ users: User[], nextSince: number }> => {
  const url = `${baseUrl}users?since=${since}&per_page=${pageSize}`;
  // todo: normally we would have own fetch wrapper and/or use Axios, and have common errors handling for most cases.
  return fetch(url)
    .then(response => {
      if (response.ok) {
        const linkHeader = response.headers.get('Link') || '';
        const nextSince = parseNextSinceValue(linkHeader);
        return response.json()
          .then(json => Promise.resolve({
              nextSince,
              users: (json as UserDto[]).map(makeUser),
            })
          );
      } else {
        throw new Error(`Server response status ${response.status}`);
      }
    })
    .catch(error => {
      console.error('Failed to load users list.', error.message);
      return Promise.resolve({users: [], nextSince: 0});
    });
};

export const getUserDetails = (username: string): Promise<UserDetails> => {
  const url = `${baseUrl}users/${username}`;
  return fetch(url)
    .then(response => {
        if (response.ok) {
          return response.json()
            .then(json => Promise.resolve(makeUserDetails(json as UserDetailsDto)));
        } else {
          throw new Error(`User may not exist. Server response code ${response.status}.`);
        }
      }
    )
    .catch(error => {
      return Promise.reject(new Error(`Failed to load user details. ${error.message}`));
    });
};
