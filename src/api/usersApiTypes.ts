// todo: find out github model definitions, which fields are mandatory
interface UserDto {
  id: number,
  login: string,
  avatar_url?: string
}

interface UserDetailsDto extends UserDto {
  name?: string,
  company?: string,
  blog?: string,
  email?: string,
  location?: string,
}
